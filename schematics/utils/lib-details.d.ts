export declare const pkgName = "@syncfusion/ej2-angular-schedule";
export declare const pkgVer = "^17.1.48";
export declare const moduleName = "ScheduleModule, RecurrenceEditorModule";
export declare const themeVer = "~17.1.48";
